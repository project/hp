This module integrates the Human Presence API to protect Drupal forms.

More info: https://humanpresence.io

The protected form is altered by `hp_form_alter` ctools plugins. These plugins determine the behavior of the form in case of failed/successful HP validation (but in theory can do anything you want, they are just form_alter hooks.) 

To implement your own plugin, create a `plugins/hp_form_alter` folder in your module folder and add the plugin file there. The plugin can have the following keys: 
* `'title'` The human readable plugin title.
* `'form_alter_callback'` is the actual form alter callback called by `hp_form_alter()`. It gets `$form`, `$form_state`, `$form_id` and `$config` as arguments. `$config` is the plugin configuration. The recommended way to implement a plugin is to add a `$form['hp'][$form_id]` form element with `'#element_validate'` callbacks. (Adding a callback to `$form['#validate']` does not work when the clicked button has validate callbacks.)
* `'configuration_form'` is the plugin's configutation form callback displayed on the Human Presence admin form. It receives the complete `$form`, the `$form_state` and `$config` as argurments and should return the config form array. Usually you add `#validate` or `#element_validate` callbacks to the form here. To add these callbacks to the plugin file (and not to the .module file), add `_hp_load_ctools_plugin` as the first `#validate` or `#element_validate` callback to make sure your plugin file is loaded. 
* `'access'` specifies the access callback to decide if the plugin is available at all. The callback receives the `$form_id` as argument. It is called both from the admin form and during form altering. It can be used to check available modules like in the case of plugins/hp_form_alter/captcha.inc.

See plugins/hp_form_alter/fail_validation.inc and plugins/hp_form_alter/captcha.inc in this module for an example.

You also need to add

```
/**
 * Implements hook_ctools_plugin_directory().
 */
function MY_MODULE_ctools_plugin_directory($module, $plugin) {
  if ($module == 'hp') {
    return 'plugins/' . $plugin;
  }
}
```
to your .module file.

There are 2 plugins shipped with this module:
* Simple fail form validation plugin. It does what its name suggests if the HP confidence is below the configured threshold.
* If CAPTCHA module is enabled, the CAPTCHA plugin displays a CAPTCHA after form validation if HP confidence is below the configured threshold. CAPTCHA configuration still happens on the CAPTCHA admin form. If CAPTCHA is not enabled for a form, the CAPTCHA plugin won't be available for HP.
