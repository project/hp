<?php
/**
 * @file
 * Documents hooks defined by the Human Presence module.
 */


/**
 * Defines form info available for protection via Human Presence.
 *
 * @return array
 *   An array of form information keyed by form ID whose values are associative
 *   arrays with the following keys:
 *   - label: a label to use for the form in the user interface.
 *
 * @see hp_protected_forms()
 * @see hp_hp_protected_forms()
 */
function hook_hp_form_info() {
  return array(
    'example_form_id' => array(
      'label' => t('Example form'),
    ),
  );
}

/**
 * Allows modules to alter the array of Human Presence form info.
 */
function hook_hp_form_info_alter(&$forms) {
  $forms['example_form_id']['label'] = t('My form label');
}
