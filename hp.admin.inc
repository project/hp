<?php
/**
 * @file
 * Administrative page callbacks and form for the Human Presence module.
 */

/**
 * Builds the Human Presence settings form.
 */
function hp_settings_form($form, &$form_state) {
  // Determine if the settings are in the database; if they are not but have
  // values set, we disable the form fields so the values will not be saved to
  // the database on submission.
  $api_key = hp_api_key();
  $api_key_in_database = unserialize(db_query("SELECT value FROM {variable} WHERE name = 'hp_api_key'")->fetchField());

  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('API credentials'),
    '#description' => t('If you do not have a Human Presence account yet, you can <a href="!register_url" target="_blank">register an account</a> with a 14-day free trial to evaluate it.', array('!register_url' => hp_registration_url())) . '<br />' . t('If you forgot your account details or cannot find your API key, email support at drupalsupport@humanpresence.io.'),
  );

  // Show a message informing the user of best practices to not store API
  // credentials in the database if necessary.
  if (empty($api_key) || (!empty($api_key) && !empty($api_key_in_database))) {
    $form['credentials']['#description'] .= '<div class="messages warning">' . t('It is best practice to store API credentials outside of the database and your source code repository. Consider setting the credential variable hp_api_key as a server environment variable and bringing it into your Drupal configuration via the $conf array in your settings.php file instead.') . '</div>';
  }

  if (!empty($api_key) && empty($api_key_in_database)) {
    $form['credentials']['hp_api_key'] = array(
      '#type' => 'item',
      '#title' => t('API key'),
      '#markup' => check_plain($api_key),
    );
  }
  else {
    $form['credentials']['hp_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('API key'),
      '#default_value' => $api_key,
    );

    // Include a company and username field if the API key isn't specified.
    $form['credentials']['request'] = array(
      '#type' => 'fieldset',
      '#title' => t('Request API key using your company and username'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['credentials']['request']['hp_company'] = array(
      '#type' => 'textfield',
      '#title' => t('Company'),
      '#default_value' => hp_company(),
    );
    $form['credentials']['request']['hp_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => hp_username(),
    );
    $form['credentials']['request']['validate'] = array(
      '#type' => 'button',
      '#value' => t('Validate'),
      '#attributes' => array(
        'onclick' => 'return hp_request_api_key();',
      ),
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'hp') . '/hp_admin.js'),
      ),
    );
  }

  // Allow the admin to enable / disable Human Presence site-wide and then to
  // disable it for certain user roles.
  $form['monitor'] = array(
    '#type' => 'fieldset',
    '#title' => t('Human Presence monitoring'),
  );
  $form['monitor']['hp_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Human Presence monitoring via its JavaScript user interaction tracker.'),
    '#default_value' => hp_enabled(),
  );
  $form['monitor']['hp_role_exclusions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Do not monitor users with the following roles:'),
    '#options' => user_roles(),
    '#default_value' => hp_role_exclusions(),
    '#states' => array(
      'visible' => array(
        ':input[name="hp_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Create the options and default value arrays for protected forms.
  $forms = hp_protected_forms();
  $form['hp_protected_forms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Protected forms'),
    '#states' => array(
      'visible' => array(
        ':input[name="hp_enabled"]' => array('checked' => TRUE),
      ),
    ),
    '#tree' => TRUE,
  );
  ctools_include('plugins');
  $hp_form_alter_plugins = ctools_get_plugins('hp', 'hp_form_alter');

  foreach ($forms as $form_id => $value) {
    $plugin_options = array(0 => t('- No HP on this form -'));
    foreach ($hp_form_alter_plugins as $plugin) {
      // Try to call the plugin access callback to decide if the plugin is
      // available or not.
      $access_function = ctools_plugin_load_function('hp', 'hp_form_alter', $plugin['name'], 'access');
      if (($access_function && $access_function($form_id)) || empty($access_function)) {
        $plugin_options[$plugin['name']] = $plugin['title'];
      }
    }

    $id = 'hp_protected_forms_' . $form_id;
    $form['hp_protected_forms'][$form_id] = array(
      '#type' => 'fieldset',
      '#title' => $value['label'] . ' (' . $form_id . ')',
      '#prefix' => '<div id=' . $id . '>',
      '#suffix' => '</div>',
    );
    $form['hp_protected_forms'][$form_id]['plugin'] = array(
      '#title' => t('Plugin'),
      '#type' => 'select',
      '#options' => $plugin_options,
      '#default_value' => !empty($value['protected']['plugin']) ? $value['protected']['plugin'] : 0,
      '#ajax' => array(
        'wrapper' => $id,
        'callback' => 'hp_protected_forms_ajax',
      ),
    );
    $selected_plugin = empty($value['protected']['plugin']) ? 0 : $value['protected']['plugin'];
    if (isset($form_state['values']['hp_protected_forms'][$form_id]['plugin'])) {
      $selected_plugin = $form_state['values']['hp_protected_forms'][$form_id]['plugin'];
    }
    if (!empty($selected_plugin)) {
      ctools_include('plugins');
      if ($function = ctools_plugin_load_function('hp', 'hp_form_alter', $selected_plugin, 'configuration_form')) {
        $form['hp_protected_forms'][$form_id] = array_merge($form['hp_protected_forms'][$form_id], $function($form, $form_state, $value['protected']));
      }
    }
  }

  return system_settings_form($form);
}

/**
 * AJAX callback to load the configuration form for the plugin.
 */
function hp_protected_forms_ajax($form, $form_state) {
  $parents = $form_state['triggering_element']['#array_parents'];
  array_pop($parents);
  $triggering_element = drupal_array_get_nested_value($form, $parents);
  return $triggering_element;
}
