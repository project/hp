<?php

/**
 * CAPTCHA hp_form_alter plugin.
 */

$plugin = array(
  'title' => t('CAPTCHA after HP fail'),
  'form_alter_callback' => 'hp_captcha_form_alter',
  'configuration_form' => 'hp_captcha_configuration_form',
  'access' => 'hp_captcha_access',
);

/**
 * Form alter callback.
 */
function hp_captcha_form_alter(&$form, &$form_state, $form_id, $config) {
  if (isset($form['captcha']) && empty($form_state['hp_captcha'])) {
    unset($form['captcha']);
  }
  $form['hp_captcha'] = array(
    '#type' => 'container',
    '#element_validate' => array(
      // We need this #validate callback to load this plugin file during
      // validation. See README.md.
      '_hp_load_ctools_plugin',
      'hp_captcha_form_validation',
    ),
    '#hp_config' => $config,
  );
}

/**
 * Displays CAPTCHA if Human Presence is not 100% confident.
 */
function hp_captcha_form_validation($element, &$form_state) {
  if (!empty($form_state['hp_captcha'])) {
    return;
  }
  // Fetch the Human Presence check response.
  $response = hp_check_session();

  // If the response is 100% sure the user is a human, do not prevent submission.
  if (!empty($response) && $response->signal == 'HUMAN' && $response->confidence >= $element['#hp_config']['minimal_confidence']) {
    return;
  }

  // Otherwise fail the detection check and prevent form submission.
  // Can't use form_set_error() since that prevents form rebuilding.
  drupal_set_message(t('Sorry, we could not process your submission at this time. Please solve the CAPTCHA below.'), 'error');
  watchdog('hp', 'Suspicious form submission blocked: <pre>@response</pre>', array('@response' => print_r($response, TRUE)), WATCHDOG_NOTICE);
  $form_state['hp_captcha'] = TRUE;
  $form_state['rebuild'] = TRUE;
  // Store the hp_captcha $form_state flag.
  form_set_cache($form_state['complete form']['#build_id'], $form_state['complete form'], $form_state);
}

/**
 * Configuration form.
 */
function hp_captcha_configuration_form($complete_form, $form_state, $config) {
  $config_form = array();
  $config_form['minimal_confidence'] = array(
    '#type' => 'textfield',
    '#title' => t('The minimal confidence below which we display a CAPTCHA.'),
    '#description' => t('Should be a number between 0 and 100. 0 means "never show CAPTCHA after form submission".'),
    '#default_value' => $config['minimal_confidence'] ?: 100,
    '#required' => TRUE,
    '#element_validate' => [
      'hp_minimal_confidence_validate',
    ],
  );
  return $config_form;
}

/**
 * Access callback for the plugin.
 */
function hp_captcha_access($form_id) {
  if (!module_exists('captcha')) {
    return FALSE;
  }
  // Get forms supported by CAPTCHA.
  module_load_include('inc', 'captcha', 'captcha');
  $captcha_points = captcha_get_captcha_points();
  if (empty($captcha_points[$form_id])) {
    return FALSE;
  }
  if (empty($captcha_points[$form_id]->captcha_type)) {
    return FALSE;
  }
  return TRUE;
}
