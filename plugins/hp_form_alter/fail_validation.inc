<?php

/**
 * Simple form validation failure hp_form_alter plugin.
 */

$plugin = array(
  'title' => t('Fail form validation'),
  'form_alter_callback' => 'hp_fail_validation_form_alter',
  'configuration_form' => 'hp_fail_validation_configuration_form',
);

/**
 * Form alter callback.
 */
function hp_fail_validation_form_alter(&$form, &$form_state, $form_id, $config) {
  $form['hp'][$form_id] = array(
    '#type' => 'container',
    '#element_validate' => array(
      // We need this #validate callback to load this plugin file during
      // validation. See README.md.
      '_hp_load_ctools_plugin',
      'hp_fail_form_validation',
    ),
    '#hp_config' => $config,
  );
}

/**
 * Makes form validation fail if Human Presence is not 100% confident.
 */
function hp_fail_form_validation($element, &$form_state) {
  // Fetch the Human Presence check response.
  $response = hp_check_session();

  // If the response is 100% sure the user is a human, do not prevent submission.
  if (!empty($response) && $response->signal == 'HUMAN' && $response->confidence >= $element['#hp_config']['minimal_confidence']) {
    return;
  }

  // Otherwise fail the detection check and prevent form submission.
  form_set_error('', t('Sorry, we could not process your submission at this time. Please try again later.'));
  watchdog('hp', 'Suspicious form submission blocked: <pre>@response</pre>', array('@response' => print_r($response, TRUE)), WATCHDOG_NOTICE);
}

/**
 * Configuration form.
 */
function hp_fail_validation_configuration_form($complete_form, $form_state, $config) {
  $config_form = array();
  $config_form['minimal_confidence'] = array(
    '#type' => 'textfield',
    '#title' => t('The minimal confidence needed to pass form validation.'),
    '#description' => t('Should be a number between 0 and 100. 0 means always pass validation.'),
    '#default_value' => $config['minimal_confidence'] ?: 100,
    '#required' => TRUE,
    '#element_validate' => [
      'hp_minimal_confidence_validate'
    ],
  );
  return $config_form;
}
