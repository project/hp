/**
 * @file
 * Custom JavaScript for the Human Presence admin settings form.
 */

function hp_request_api_key() {
  // Fetch the Company value from the form.
  var company = jQuery("#edit-hp-company").attr("value");
  if (company == "") {
    jQuery("#edit-hp-company").focus();
    alert(Drupal.t("Please enter your Company."));
    return false;
  }

  // Fetch the Username value from the form.
  var username = jQuery("#edit-hp-username").attr("value");
  if (username == "") {
    jQuery("#edit-hp-username").focus();
    alert(Drupal.t("Please enter your Username."));
    return false;
  }

  // Request the API key for the given user.
  jQuery.post("https://ellipsis-user-micro.ellipsis.cloud/api/validate?c=" + company + "&u=" + username, function (data) {
    // Alert the user if we received an error response.
    if (data.error) {
      alert(Drupal.t("Your Company and Username did not match. Please try again."));
    }
    else if (data.apiKey) {
      jQuery("#edit-hp-api-key").attr("value", data.apiKey);
      jQuery("#edit-request").find(".fieldset-title").click();
      alert(Drupal.t("API key fetched. Save the form to begin using Human Presence."));
    }
  });

  return false;
}
